<?php

return array(
	'fontDefault' => array(
		'font' => array(
		  'name'      =>  'Arial',
		  'size'      =>  10,
		  'bold'      =>  false
		)
	),
	'borderStyleSolid' => array(
        'borders' => array(
            'top'   => array(
                'style' => 'solid'
            ),
        )
    ),
    'meses' => array(
    	'Enero', 'Febrero', 'Marzo',
    	'Abril','Mayo','Junio', 'Julio',
    	'Agosto','Septiembre','Octubre',
    	'Noviembre','Diciembre'
	),
	'generalSettings' => array(
		'background' => '#909090',
		'fontColor' => '#ffffff',
		'fontFamily' => 'Arial',
		'fontSize' => 10,
		'Alignament' => 'left',
		'VAlignament' => 'middle',
		'fontWeight' => 'bold'
	),
	'alphabeth' => array(
			"A","B","C","D","E","F","G","H","I",
			"J","K","L","M","N","O","P","Q","R",
			"S","T","U","V","W","X","Y","Z"
		)

);
